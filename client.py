import requests
import random
import logging
import socket
import threading
import json

class Client():
	"""
	Class that creates the client. Each client has a name and save the list of active users.
	"""
	def __init__(self, name, host='', port=65432):
		self.host = host
		self.port = random.randint(50000, 65000)
		self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		self.connections = set()
		self.name = name
		self.active_users = {}
		
	def register_client(self):
		"""
	    Sends a registration request to the server with the client's name and port.
	    Returns a tuple containing the response message and the status code.
	    """
		response = requests.get(f"http://localhost:5000/register/{self.name}/{self.port}")
		msg = response.text
		status_code = response.status_code
		return (msg, status_code)

	def get_active_users(self):
		"""
	    Retrieves the list of active users from the server.
	    Removes the current client from the active users list.
	    Logs the active users and returns the updated active users dictionary.
	    """
		self.active_users = dict(requests.get("http://localhost:5000/get-client-list").json())
		self.active_users.pop(self.name)
		logging.debug(self.active_users)
		return self.active_users

	def connect_to_active_users(self):
		"""
	    Connects the client to the active users.
	    Prints a message if there are no active users currently available.
	    For each active user, establishes a socket connection, adds it to the connections set,
	    starts a receive thread, and prints the connection status.
	    If connection fails, prints an error message.
	    """
		if len(self.active_users) == 0:
			print("It seems there's no one else right now...")
		for name, conf in self.active_users.items():

			socket_conn = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
			try:
				print(f"Connecting to {name}")
				socket_conn.connect(('', conf['port']))
				self.connections.add(socket_conn)
				print(f"Connected to {name}")
				receive_thread = threading.Thread(target=self.on_message, args=[socket_conn])
				receive_thread.start()
			except socket.error as e:
				print(f"Failed to connect to {name}. Error: {e}")

	def listen(self):
		"""
	    Listens for incoming connections on the specified host and port.
	    Accepts incoming connections, adds them to the connections set,
	    starts a receive thread, and prints the connection status.
	    Stops listening when an exception occurs.
	    """
		self.socket.bind(('', self.port))
		self.socket.listen(10)
		print(f"Listening for connections on {self.host}:{self.port}")

		while True:
			try:
				connection, address = self.socket.accept()
				self.connections.add(connection)
				print(f"Accepted connection from {address}")
				receive_thread = threading.Thread(target=self.on_message, args=[connection])
				receive_thread.start()
			except Exception:
				break
		print("Stopped listening service")

	def send_message(self, data):
		"""
	    Sends the provided data to all connected clients in the connections set.
	    Serializes the data to JSON and sends it over the socket connection.
	    Prints an error message if sending data fails.
	    """
		for connection in self.connections:
			try:
				connection.sendall(json.dumps(data).encode())
			except socket.error as e:
				print(f"Failed to send data. Error: {e}")

	def on_message(self, *args):
		"""
	    Handles incoming messages from a client specified by the connection argument.
	    Receives messages, decodes them from JSON format, and prints them.
	    If an exception occurs during message handling, prints a connection closed message
	    and removes the connection from the connections set.
	    """
		connection = args[0]
		peer_name = connection.getpeername()
		while True:
			try:
				message = json.loads(connection.recv(4096).decode())
				print(f"{message['name']}: {message['msg']}")
			except Exception:
				print(f"Connection with {peer_name} closed")
				self.connections.discard(connection)
				break
			
	def start(self):
		"""
	    Starts the client by creating and starting a thread for the listen method.
	    """
		listen_thread = threading.Thread(target=self.listen)
		listen_thread.start()

	def stop(self):
		"""
	    Sends an unregister request to the server to unregister the client.
	    Closes all connections and shuts down the client's socket.
	    """
		print(requests.get(f"http://localhost:5000/unregister/{self.name}").text)
		print("Closing connections")
		for connection in list(self.connections):
			connection.shutdown(socket.SHUT_RDWR)
		self.socket.shutdown(socket.SHUT_RDWR)
		