# Chat with Peers
### Usage
```
pip install -r requirements.txt
```
In two different windows start server and client:
    - ``` python main.py --server ```
    - ``` python main.py --client ```
    
You should start one server and multiple clients, then you are ready to start enjoying your chats.