import os
import argparse
import logging

from functools import partial
from client import Client

def start_conversation(client):
	client.connect_to_active_users()
	joining_msg = {
		"name":client.name,
		"msg":f"Hi everyone, I'm {client.name}"
	}
	client.send_message(joining_msg)

	try:
		msg = input("Send a message to everyone (type exit to stop the conversation)\n")
		while (msg != "exit"):
			message = {
				"name":client.name,
				"msg":msg
			}
			client.send_message(message)
			msg = input()
	except KeyboardInterrupt:
		pass
	except EOFError:
		pass
	except Exception:
		pass

	client.stop()
	return

def retry():
	name = input("Please, enter another name ").strip()
	client = Client(name)
	welcome_msg, status_code = client.register_client()
	print(welcome_msg)
	return (client, status_code)

def start_client():
	print("Hello user, this is Chat with Peers, a peer-to-peer chat that gives you the possibility to chat"\
		" with people on the same network.\n")
	name = input("What is your name? ").strip()
	print("When you start a conversation you can chat with every active user in broadcast.\n")
	try:
		client = Client(name)
		welcome_msg, status_code = client.register_client()
		print(welcome_msg)
		while status_code == 400:
			client, status_code = retry()
		client.start()
		client.get_active_users()
		start_conversation(client)
	except Exception as e:
		logging.error("Network error", e)

fn_mappings = {
	'server':{
		'f':partial(os.system,"flask --app server run")
	},
	'client':{
		'f':partial(start_client)
	}
}

parser = argparse.ArgumentParser()
group = parser.add_mutually_exclusive_group()
group.add_argument(
	'--server',
	'-s',
	dest='server',
	help="Option to start the server",
	action='store_true'
)
group.add_argument(
	'--client',
	'-c',
	dest='client',
	help="Option to start the client",
	action='store_true'
)

if __name__=="__main__":
	args = parser.parse_args()
	for elem in fn_mappings.keys():
		if getattr(args, elem):
			fn_mappings[elem]["f"]()
