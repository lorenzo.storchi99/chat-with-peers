from flask import Flask
from flask import request
from flask import Response
from markupsafe import escape

app = Flask(__name__)

active_users = {}
    
@app.route("/register/<username>/<port>")
def register_client(username, port):
    """
    Once a client sends a request to this url specifing his name, it is registered and saved in active_users list.
    A welcome message is sent back as well.
    """
    if escape(username) not in active_users:
        active_users[f"{escape(username)}"] = {
            "ip":request.environ.get('REMOTE_ADDR'),
            "port":int(port)
        }
        print(active_users)
    else:
        return f"Name already taken", 400    # 1 Error, name already taken

    return Response(f"Hello {escape(username)}, thank you for using this service", status=200)   # 0 It's all ok, you can proceed

@app.route("/unregister/<username>")
def uregister_client(username):
    """
    Once a client sends a request to this url specifing his name, it is unregistered and deleted from active_users list.
    """
    try:
        active_users.pop(username)
    except Exception:
        return f"Name already unregistered", 400

    return f"Unregistered from server", 200

@app.route("/get-client-list")
def get_client_list():
    """
    Simple function that sends the list of active_users.
    """
    return active_users